import { AnimationType } from "../../constants/enums";
import { cloneArray } from "../../utils";

export function getShellSortAnimations(array) {
  const animations = [];
  if (array.length <= 1) return array;

  shellSort(cloneArray(array), animations);
  return animations;
}

function shellSort(array, animations) {
  var increment = parseInt(array.length / 2);

  while (increment > 0) {
    for (var i = increment; i < array.length; i++) {
      var j = i;
      var tmp = array[i].height;

      while (j >= increment && array[j - increment].height > tmp) {
        animations.push({
          bars: [j, i],
          type: AnimationType.COMPARE
        });
        array[j].height = array[j - increment].height;
        animations.push({
          bars: [j],
          height: array[j - increment].height,
          type: AnimationType.RESIZE
        });
        animations.push({
          bars: [j, i],
          type: AnimationType.DECOMPARE
        });
        j = j - increment;
      }

      animations.push({
        bars: [j],
        height: tmp,
        type: AnimationType.RESIZE
      });
      array[j].height = tmp;
    }

    if (increment === 2) {
      increment = 1;
    } else {
      increment = parseInt((increment * 5) / 11);
    }
  }
}

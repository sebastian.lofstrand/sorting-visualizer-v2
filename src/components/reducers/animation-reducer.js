import * as SORTING from "../../constants/sorting";
import {
  getShellSortAnimations,
  getSelectionSortAnimations,
  getInsertionSortAnimations,
  getMergeSortAnimations,
  getQuickSortAnimations
} from "../algorithms";

const animationReducer = (state, action) => {
  const array = action.array;

  switch (action.type) {
    case SORTING.INSERTION:
      return {
        ...state,
        animations: getInsertionSortAnimations(array)
      };
    case SORTING.MERGE:
      return {
        ...state,
        animations: getMergeSortAnimations(array)
      };
    case SORTING.SELECTION:
      return {
        ...state,
        animations: getSelectionSortAnimations(array)
      };
    case SORTING.SHELL:
      return {
        ...state,
        animations: getShellSortAnimations(array)
      };
    case SORTING.QUICK:
      return {
        ...state,
        animations: getQuickSortAnimations(array)
      };
    case SORTING.RESET:
      return {
        ...state,
        animations: []
      };
    default:
      return state;
  }
};

export default animationReducer;

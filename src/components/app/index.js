import React from "react";

import Navbar from "./navbar";
import Visualizer from "../visualizer";

import * as SORTING from "../../constants/sorting";

import { stateSchema, validationStateSchema } from "./validation";
import { newArray } from "../../utils";
import useForm from "../session/use-form";
import animationReducer from "../reducers/animation-reducer";

const App = () => {
  const { state, handleOnChange } = useForm(stateSchema, validationStateSchema);
  const [array, setArray] = React.useState([]);
  const [data, dispatch] = React.useReducer(animationReducer, {
    animations: []
  });

  const resetArray = () => {
    setArray(newArray(state.arraySize.value));
    dispatch({ type: SORTING.RESET, array });
  };

  const updateVh = () => {
    let vh = window.innerHeight * 0.01;
    document.documentElement.style.setProperty("--vh", `${vh}px`);
  };

  React.useEffect(() => {
    setArray(newArray(state.arraySize.value));

    // We listen to the resize event
    window.addEventListener("resize", updateVh());

    return window.removeEventListener("resize", updateVh());
    // eslint-disable-next-line
  }, []);

  const sorting = type => {
    dispatch({ type, array });
  };

  return (
    <div className="wrapper">
      <Navbar
        handleOnChange={handleOnChange}
        sorting={sorting}
        state={state}
        resetArray={resetArray}
      />
      <Visualizer
        animations={data.animations}
        array={array}
        setArray={setArray}
        delay={100 - state.animationSpeed.value}
      />
    </div>
  );
};

export default App;

import React from "react";
import * as SORTING from "../../constants/sorting";

const Navbar = ({ state, handleOnChange, resetArray, sorting }) => {
  const algorithms = [
    {
      title: "Insertion",
      algorithm: SORTING.INSERTION
    },
    {
      title: "Merge",
      algorithm: SORTING.MERGE
    },
    {
      title: "Selection",
      algorithm: SORTING.SELECTION
    },
    {
      title: "Shell",
      algorithm: SORTING.SHELL
    },
    {
      title: "Quick",
      algorithm: SORTING.QUICK
    }
  ];

  return (
    <>
      <nav>
        <form>
          <header>
            <span></span>
            Visualizer
          </header>
          <ul>
            <li>
              <span>Animation speed</span>
            </li>
            <li>
              <span>
                <input
                  className={"custom-range w-100"}
                  min="1"
                  max="99"
                  step="1"
                  type="range"
                  name="animationSpeed"
                  onChange={handleOnChange}
                  value={state.animationSpeed.value}
                />
              </span>
            </li>
            <li>
              <span>Size</span>
            </li>
            <li>
              <span>
                <input
                  className={"custom-range w-100"}
                  min="10"
                  max="100"
                  step="1"
                  type="range"
                  name="arraySize"
                  onChange={handleOnChange}
                  value={state.arraySize.value}
                />
              </span>
            </li>
            <li>
              <span>Algorithm</span>
            </li>
            <li>
              <span className="py-1 px-1">
                <select
                  className="form-control"
                  name="algorithm"
                  onChange={handleOnChange}
                  value={state.algorithm.value}
                >
                  {algorithms.map((a, idx) => (
                    <option key={idx} value={a.algorithm}>
                      {a.title}
                    </option>
                  ))}
                </select>
              </span>
            </li>
            <li>
              <span className="link reset" onClick={() => resetArray()}>
                Reset
              </span>
            </li>
            <li>
              <span
                className="link simulate"
                onClick={() => sorting(state.algorithm.value)}
              >
                Simulate
              </span>
            </li>
          </ul>
        </form>
      </nav>
    </>
  );
};

export default Navbar;

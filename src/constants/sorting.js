export const MERGE = "merge-sort";
export const INSERTION = "insertion-sort";
export const SELECTION = "selection-sort";
export const SHELL = "shell-sort";
export const BUBBLE = "bubble-sort";
export const QUICK = "quick-sort";

export const RESET = "reset";
